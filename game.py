from random import randint
player_name = input('Hi! What is your name? ')

guess_number = 1
month_number = randint(1, 12)
year_number = randint(1970, 2000)
try_times = 5


while try_times > 0:
    if guess_number == 1:
        print("Guess", guess_number, ":", player_name, 
        "were you", "born on", month_number, "/", year_number, "?")
        response = input('yes or no?  ')


        if response == 'yes':
            print('I knew it!')
            exit()
        else:
            print('Drat! Lemme try again!')
                
        if try_times > 5:
            print('I have other things to do. Good bye.')
            exit()
